/**
 * Idea from http://imgur.com/a/megDZ
 */

// size of the image
int width = 480;
int height = 360;

// the chance any single point has of starting alive
double initChance = 0.4;

// how many spaces have to be alive around for the cell to be alive after smoothing
int liveThreshold = 4;

// colours for alive and dead cells
final int ALIVE = 255;
final int DEAD = 0;


int[][] map = new int[width][height];

void setup(){
    surface.setSize(width, height);
    
    noLoop();
}

void draw(){
    init();
    smoothing();
    drawAll();
}

void drawAll(){
    for(int x = 0; x < width; x++){
        for(int y = 0; y < height; y++){
            drawPoint(x, y);
        }
    }
}

void drawPoint(int x, int y){
    if(map[x][y] == 1){
        stroke(ALIVE);
    }else{
        stroke(DEAD);
    }
    point(x, y);
}

void init(){
    for(int x = 0; x < width; x++){
        for(int y = 0; y < height; y++){
            map[x][y] = random(1) <= initChance ? 1 : 0;
            //drawPoint(x, y);
        }
    }    
}

void smoothing(){
    int[][] smoothed = new int[width][height];
    
    for(int x = 0; x < width; x++){
        for(int y = 0; y < height; y++){
            int living = 0;

            int dx = x-1;
            if(dx < 0){
                dx = width-1;
            }
            int ddx = x+1;
            if(ddx == width){
                ddx = 0;
            }

            int dy = y-1;
            if(dy < 0){
                dy = height-1;
            }
            int ddy = y+1;
            if(ddy == height){
                ddy = 0;
            }
            // top left
            living += map[dx][dy];
            // top middle
            living += map[x][dy];
            // top right
            living += map[ddx][dy];
            
            // center left
            living += map[dx][y];
            // center middle
            living += map[x][y];
            // center right
            living += map[ddx][y];
            
            // bottom left
            living += map[dx][ddy];
            // bottom middle
            living += map[x][ddy];
            // bottom right
            living += map[ddx][ddy];
            
            smoothed[x][y] = living >= liveThreshold ? 1 : 0;
        }
    }
    map = smoothed;
}
    