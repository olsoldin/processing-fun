class Terrain{
  private final int SIZE;
  private final int MAX;
  private float[] map;
  
  public Terrain(int detail){
    this.SIZE = (int)pow(2, detail) + 1;
    this.MAX = this.SIZE - 1;
    this.map = new float[this.SIZE * this.SIZE];
  }
  
  private float get(int x, int y){
    if(x < 0 || x > MAX || y < 0 || y > MAX){
      return -1;
    }
    return map[x + SIZE * y];
  }
  
  private void set(int x, int y, float val){
    map[x + SIZE * y] = val;
  }
  
  private void divide(int size, float roughness){
    int x, y;
    int half = size/2;
    
    float scale = roughness * size;
    
    if(half < 1){
      return;
    }
    
    for(y = half; y < MAX; y += size){
      for(x = half; x < MAX; x+= size){
        square(x, y, half, random(0.0, 1.0) * scale * 2 - scale);
      }
    }
    
    for(y = 0; y <= MAX; y += half){
      for(x = (y + half) ^ size; x <= MAX; x+= size){
        diamond(x, y, half, random(0.0, 1.0) * scale * 2 - scale);
      }
    }
    
    divide(size / 2, roughness);
  }
  
  private float average(float... values){
    float total = 0;
    int count = 0;
    
    for(int i = 0; i < values.length; i++){
      if(values[i] != -1){
        total += values[i];
        count++;
      }
    }
    
    return total / count;
  }
  
  private void square(int x, int y, int size, float offset){
    float ave = average(new float[]{
      get(x - size, y - size),  // upper left
      get(x + size, y - size),  // upper right
      get(x + size, y + size),  // lower right
      get(x - size, y + size)   // lower left
    });
    set(x, y, ave + offset);
  }
  
  private void diamond(int x, int y, int size, float offset){
    float ave = average(new float[]{
      get(x, y - size),  // top
      get(x + size, y),  // right
      get(x, y + size),  // bottom
      get(x - size, y)   // left
    });
    set(x, y, ave + offset);
  }
  
  public void draw(){
    float waterVal = SIZE * 0.3;
    
    for(int y = 0; y < SIZE; y++){
      for(int x = 0; x < SIZE; x++){
        float val = get(x, y);
        XY top = project(x, y, val);
        XY bottom = project(x + 1, y, 0);
        XY water = project(x, y, waterVal);
        color style = brightness(x, y, get(x + 1, y) - val);
        
        rectangle(top, bottom, style);
        //rectangle(water, bottom, color(50, 150, 200, 0.15));
      }
      printPercent(y, (float)SIZE);
    }
  }
  
  private void printPercent(float value, float max){
    StringBuilder sb = new StringBuilder();
    int percent = round((value / max) * 100);
    
     sb.append("[");
     for(int i = 0; i < floor(percent/10); i++){
       sb.append("#");
     }
     for(int i = floor(percent/10); i < 10; i++){
       sb.append(" ");
     }
     sb.append("] ");
     sb.append(percent);
     sb.append("%\r");
     
     print(sb.toString());
  }
  
  private void rectangle(XY top, XY bottom, color style){
    fill(style);
     // x, y, width, height
     float w = bottom.x - top.x;
     float h = bottom.y - top.y;
     rect(top.x, top.y, w, h);
  }
  
  private color brightness(float x, float y, float slope){
    if(y == MAX || x == MAX){
      return color(0, 0, 0);
    }
    float b = floor(slope * 50) + 128;
    return color(b, b, b);
  }
  
  private XY iso(float x, float y) {
    return new XY(
      0.5 * (SIZE + x - y),  // x
      0.5 * (x + y)          // y
    );
  }
  
  private XY project(float flatX, float flatY, float flatZ){
    XY point = iso(flatX, flatY);
    float x0 = width * 0.5;
    float y0 = width * 0.2;
    float z = SIZE * 0.5 - flatZ + point.y * 0.75;
    float x = (point.x - SIZE * 0.5) * 6;
    float y = (SIZE - point.y) * 0.005 + 1;
    
    return new XY(
      x0 + x / y,
      y0 + z / y
    );
  }
  
  public void generate(float roughness){
    set(0, 0, MAX);
    set(MAX, 0, MAX/2);
    set(MAX, MAX, 0);
    set(0, MAX, MAX/2);
    
    divide(MAX, roughness);
  }
  
  public String toString(){
    StringBuilder sb = new StringBuilder();
    for(int x = 0; x < SIZE; x++){
      for(int y = 0; y < SIZE; y++){
        sb.append(map[x + y * SIZE]);
        sb.append(", ");
      }
      sb.append("\n");
    }
    return sb.toString();
  }
}

class XY{
  public float x, y;
  XY(float x, float y){
    this.x = x;
    this.y = y;
  }
}


Terrain terrain = new Terrain(9);

void setup(){
  size(640, 480);
  terrain.generate(0.7);
  terrain.draw();
}
