void setup() {
    size(640, 360);

    frameRate(60);
}

void draw() {
    if (frameCount % 10 == 0) {
        background(0);
        text((int)frameRate, 10, 20);
    }
    
}