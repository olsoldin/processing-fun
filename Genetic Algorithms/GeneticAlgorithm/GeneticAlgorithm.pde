String target = "to be or not to be";

DNA[] population = new DNA[100];
int generations = 0;

void setup(){
    for(int i = 0; i < population.length; i++){
        population[i] = new DNA(target);
    }
}

void draw(){
    ArrayList<DNA> matingPool = new ArrayList<DNA>();
    
    for(int i = 0; i < population.length; i++){
        population[i].fitness();
        
        // The higher fitness it has, the more representation it gets in the mating pool
        int n = int(population[i].fitness * 100);
        for(int j = 0; j < n; j++){
            matingPool.add(population[i]);
        }
    }
    
    for(int i = 0; i < population.length; i++){
        int a = int(random(matingPool.size()));
        int b = int(random(matingPool.size()));
        
        DNA parentA = matingPool.get(a);
        DNA parentB = matingPool.get(b);
        
        DNA child = parentA.crossover(parentB);
        child.mutate();
        
        population[i] = child;
        generations++;
        
        child.fitness();
        println("Generation::" + generations);
        println(child);
    }
}