class DNA {
    // Each "gene" is one element of the array. We need
    // 18 genes because "to be or not to be" is 18 chars
    // long.
    char[] genes = new char[18];
    float fitness;
    String target;
    float mutationRate = 0.01; // 1%
    
    DNA(String target) {
        this.target = target;
        for(int i = 0; i < genes.length; i++){
            // Picking randomly from a range of characters with ASCII
            // values between 32 and 128
            genes[i] = (char)random(32, 128);
        }
    }
    
    void fitness(){
        int score = 0;
        for(int i = 0; i < genes.length; i++){
            if(genes[i] == target.charAt(i)){
                score++;
            }
        }
        fitness = float(score)/target.length();    // Fitness is the % correct
    }
    
    DNA crossover(DNA partner){
        // The DNA is generated randomly in the constructor,
        // but we'll override it below with the DNA of the parents
        DNA child = new DNA(target);
        
        // Picking a random "midpoint" in the genes array
        int midpoint = int(random(genes.length));
        
        for(int i = 0; i < genes.length; i++){
            if(i > midpoint){
                child.genes[i] = genes[i];
            }else{
                child.genes[i] = partner.genes[i];
            }
        }
        
        return child;
    }
    
    void mutate(){
        for(int i = 0; i < genes.length; i++){
            if(random(1) < mutationRate){
                // mutate random genes
                genes[i] = (char)random(32, 128);
            }
        }
    }
    
    
    @Override
    public String toString(){
        return "Fitness:: " + fitness + "\nGenes  :: " + new String(genes);
    }
}