
final int WIDTH = 640;
final int HEIGHT = 480;

void setup() {
    // "Use only numbers, not variables for size()"
    // This gets around that
    surface.setSize(WIDTH,HEIGHT);
}

void draw() {
    color c = color(255, 0, 0);
    fill(c);
    rect(20,20, 40, 40);
}