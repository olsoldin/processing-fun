// Line draw start point
int connect_x = 0;
int connect_y = 0;

Boolean drawOn = false;

// ---------------------------------------------------------------
// Note the use in dynamic visualisations of the two functions
// SETUP and DRAW
//

// ---------------------------------------------------------------
// SETUP - The function which initialises a dynamic visualisation

void setup ()
{
  background(#a7ffa7);  // What happens if you move this into DRAW?
  size(200,200);
  strokeWeight(1);
  stroke(128);
  frameRate(30);
}

// ----------------------------------------------------------------
// DRAW - A function which is called repeatedly over the lifetime
//        of the sketch activation, thus allowing for dynamic
// changes to be seen on the display

void draw ()
{
  if (drawOn)
  {
    line (connect_x, connect_y, mouseX, mouseY);
  }
}

void mousePressed ()
{
  drawOn = !drawOn;
  connect_x = mouseX;
  connect_y = mouseY;
}
