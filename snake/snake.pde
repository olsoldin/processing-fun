ArrayList<Integer> x = new ArrayList<Integer>();
ArrayList<Integer> y = new ArrayList<Integer>();

int w = 30;
int h = 30;
int boxSize = 20;
int dir = 2;
int appleX = 12;
int appleY = 10;
int[] dx = {0, 0, 1, -1};
int[] dy = {1, -1, 0, 0};
boolean gameOver = false;

void setup(){
  size(w * boxSize, h * boxSize);
  x.add(5);
  y.add(5);
}

void draw(){
  background(255);
  for(int i = 0; i < w; i++){
    line(i * boxSize, 0, i * boxSize, height);
  }
  for(int i = 0; i < h; i++){
    line(0, i * boxSize, width, i* boxSize);
  }
  fill(0,255,0);
  for(int i = 0; i < x.size(); i++){
    rect(x.get(i) * boxSize, y.get(i) * boxSize, boxSize, boxSize);
  }
  if(!gameOver){
    fill(255, 0, 0);
    rect(appleX * boxSize, appleY * boxSize, boxSize, boxSize);
    if(frameCount % 5 == 0){
      x.add(0, x.get(0) + dx[dir]);
      y.add(0, y.get(0) + dy[dir]);
      if(x.get(0) < 0 || y.get(0) < 0 || x.get(0) >= w || y.get(0) >= h){
        gameOver = true;        
      }
      for(int i = 1; i < x.size(); i++){
        if(x.get(0) == x.get(i) && y.get(0) == y.get(i)){
          gameOver = true;
        }
      }
      if(x.get(0) == appleX && y.get(0) == appleY){
        appleX = (int)random(0, w);
        appleY = (int)random(0, h);
      }else{
        x.remove(x.size() - 1);
        y.remove(y.size() - 1);
      }
    }
  }else{
    fill(0);
    textSize(30);
    textAlign(CENTER);
    text("Game Over. Press Space", width / 2, height / 2);
    if(keyPressed && key == ' '){
      x.clear();
      y.clear();
      x.add(5);
      y.add(5);
      gameOver = false;
    }
  }
}

void keyPressed(){
  int nd;
  switch(key){
    case 's':
      nd = 0;
      break;
    case 'w':
      nd = 1;
      break;
    case 'd':
      nd = 2;
      break;
    case 'a':
      nd = 3;
      break;
    default:
      nd = -1;
      break;
  }
  if(nd != -1 && (x.size() <= 1 || !(x.get(1) == x.get(0) + dx[nd] && y.get(1) == y.get(0) + dy[nd]))){
    dir = nd;
  }
}
