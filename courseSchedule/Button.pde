class Button{
  private String text;
  private boolean selected;
  private int x, y;
  private int width, height;
  
  public Button(String text, int x, int y, int width, int height){
    this.text = text;
    this.selected = false;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
  }
  
  public void draw(){
    if(selected){
      fill(0);
    }else{
      fill(255);
    }
    
    rect(x, y, this.width, this.height);
    
    if (selected){
      fill(255);
    }else{
      fill(0);
    }
    
    text(text, x + 5, y + 20 - textDescent() - 2);
  }
  
  public void mousePressed(){
    if(mouseX > x && mouseX < x + this.width
      && mouseY > y && mouseY < y + this.height){
      selected = !selected;
    }
  }
}