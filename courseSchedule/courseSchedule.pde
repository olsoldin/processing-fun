// Schedule for 3COM0261 drawn from table in schedule.tsv
//
// Format is tab delimited file as below
//
// Dates  Holiday  Mini Project  Critique  Exam
// Jan 25  0  0  1  0
// Feb 01  0  1  0  0
// :
// :
// May 24  0  0  0  1
//

// Schedule Data is read into here ... see schedule.tsv
IntTable data;

// Dates are read into here
String[] dates;

// String holding Title of graphic
String title;

// Parameters laying out interactive menu
int menuTop = 10, menuBottom = 90;
int menuLeft = 10, menuRight = 150;

// Parameters defining plot area
int plotL, plotR, plotU, plotD;

// Parameters defining areas in plot
int lec1L, lec1R;
int lec2L, lec2R;
int tutL, tutR;
int zoneWidth;

// Fonts - Large and Small
PFont lfont, sfont;

// Values recording size of input file
int colCount, rowCount;

// Booleans defining visibility of data columns
Button[] button;
// SETUP -- Preparing the visual

void setup()
{
  // Define the fonts -- Dont forget to use Tools>Create Font also!!
  lfont = loadFont("GillSansMT-60.vlw");
  sfont = loadFont("GillSansMT-16.vlw");
  
  // Define the size of the visual
  size(1000, 600);
  
  // Set the background to mid grey
  background (127);
  
  // Select the data from the file indicated
  data = new IntTable ( "schedule.tsv" );
  //Determine the number of rows, columns are fixed in this visual
  colCount = data.getColumnCount();
  rowCount = data.getRowCount();
  
  // Read the dates from the title column
  dates = data.getRowNames();
  
  button = new Button[colCount];
  int btnHeight = ((menuBottom - menuTop) / colCount);
  int btnWidth = menuRight - menuLeft;
  for(int i = 0; i < colCount; i++){
    String colTitle = data.getColumnName(i);
    
    button[i] = new Button(colTitle, menuLeft, menuTop + (i * btnHeight), btnWidth, btnHeight);
    //button[i] = new Button(colTitle, width/2, height/2 + (i * 20), btnWidth, btnHeight);
  }
  // Holidays shown by default
  button[0].selected = true;
  
  // Fix the dimensions of the plot area
  // based on the size of the visual
  plotL = 50;   plotR = width - 50;
  plotU = 100 ; plotD = height - 50;
  
  // Fix the areas of the table based on the plot area
  zoneWidth = (int)((plotR - plotL - 50)/3.0);
  lec1L = plotL + 55;                lec1R = lec1L + zoneWidth - 10;
  lec2L = plotL + zoneWidth + 55;    lec2R = lec2L + zoneWidth - 10;
  tutL = plotL + (zoneWidth*2) + 55; tutR = tutL + zoneWidth - 10;
}

// DRAW routine

void draw ()
{
  // Define the Plot Area
  strokeWeight(2);
  stroke(0);
  fill(255);
  rectMode(CORNERS);
  rect(plotL, plotU, plotR, plotD);
  
  // Draw the Title
  textFont(lfont);
  textAlign(CENTER);
  fill(#3311cc);
  text("3COM0261 Schedule", width/2, plotU - textDescent() - 10); 

  // Draw the Timeline
  strokeWeight(1);
  fill(0);
  textFont(sfont);
  textAlign(LEFT);
  
  // Draw the Semester Grid
  fill(0);
  textAlign(CENTER);
  line(plotL + 50, plotU, plotL + 50, plotD);
  float sectHeight = float(plotD - plotU)/float(rowCount);
  for (int i = 0; i <= rowCount; i++)
  {
    line(plotL, plotU + (sectHeight * i), plotR, plotU + (sectHeight * i));
    if (i < rowCount)
    {
      text(dates[i], plotL + 25, plotU + (sectHeight * (i + 0.5)) + (textAscent()/2));
    }
  }
  noStroke();
  
  textFont(sfont);
  textAlign(CENTER);

   
  // Draw the selection menu {Holiday, Critique, Projects, Exams}
  for(int j = 0; j < button.length; j++){
    button[j].draw();
  }

  // Overlay the display regions based on data and menu selection
  for (int i = 0; i < rowCount; i++ )
  {
    // Holiday overlays
    if ( button[0].selected && data.getInt(i,0) > 0)
    {
      fill (192);
      rect (lec1L, plotU + (sectHeight * i), lec1R, plotU + (sectHeight * (i+1)));
      rect (lec2L, plotU + (sectHeight * i), lec2R, plotU + (sectHeight * (i+1)));
      rect (tutL, plotU + (sectHeight * i), tutR, plotU + (sectHeight * (i+1)));
      fill (127);
      title = data.getColumnName(0);
      text ( title, lec1L + (lec1R - lec1L)/2, plotU + (sectHeight * (i+1)) - (textDescent() + 4));
      text ( title, lec2L + (lec2R - lec2L)/2, plotU + (sectHeight * (i+1)) - (textDescent() + 4));
      text ( title, tutL + (tutR - tutL)/2, plotU + (sectHeight * (i+1)) - (textDescent() + 4));
    }
    // Critique overlays
    if ( button[1].selected && data.getInt(i,1) > 0)
    {
      fill (#44cc44);
      rect (tutL, plotU + (sectHeight * i), tutR, plotU + (sectHeight * (i+1)));
      fill (0);
      title = data.getColumnName(1);
      text ( title, tutL + (tutR - tutL)/2, plotU + (sectHeight * (i+1)) - (textDescent() + 4));
    }
    // Projects overlay
    if ( button[2].selected && data.getInt(i,2) > 0)
    {
      fill (#4444cc);
      rect (lec2L, plotU + (sectHeight * i), lec2R, plotU + (sectHeight * (i+1)));
      fill (0);
      title = data.getColumnName(2);
      text ( title, lec2L + (lec2R - lec2L)/2, plotU + (sectHeight * (i+1)) - (textDescent() + 4));
    }
    // Exam overlay
    if ( button[3].selected && data.getInt(i,3) > 0)
    {
      fill (#cc4444);
      rect (plotL + 50, plotU + (sectHeight * i), plotR, plotU + (sectHeight * (i+1)));
      fill (0);
      title = data.getColumnName(3);
      text ( title, plotL + 100, plotU + (sectHeight * (i+1)) - (textDescent() + 4));
    }
  } 
}

// MousePressed utility handles interaction with the menu,
// adjusting booleans to switch on and off display elements

void mousePressed( ) {
  for(int i = 0; i < button.length; i++){
    button[i].mousePressed();
  }
}