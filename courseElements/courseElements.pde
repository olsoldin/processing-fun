// Drawing Area
size (500, 300);
background (127);

// Circles
fill (#ff7777);
ellipse (width/4, height/2, 5, 5);
fill (#0000ff);
ellipse ((width*3)/4, height/2, 200, 200);

// Text  
PFont font;
font = loadFont("ArialMT-24.vlw"); // Use Tools>Fonts to create font file .vlw
fill(255);
textFont(font);
textAlign(CENTER);
text("Data Mining", width/4, height - 20); 
text("Data Visualisation", (width*3)/4, height - 20);

