int[] angles = { 30, 10, 45, 35, 60, 38, 75, 67 };
//int[] angles = { 10, 10, 10, 10, 10, 10, 20, 30, 40, 210 };
//int[] angles = new int[360];

void setup(){
  size(640, 360);
  noStroke();
//  for(int i = 0; i < 360; i++){
//    angles[i] = 1;
//  }
}

void draw(){
  background(200);
  pieChart(300, angles);
}

void pieChart(float diameter, int[] data){
  float lastAngle = 0;
  String displayText = "";
  for (int i = 0; i < data.length; i++){
    //making the colours
    float r = map(i+1, 0, data.length*0.66, 0, 255);
    float g = map(i+1, 0, data.length/3, 0, 255);
    float b = map(i+1, 0, data.length, 0, 255);
    fill(r,g,b);
    
    //getting the angle from the center to the mouse
    float mouseAngle = 0;
    int x = mouseX - width/2; //mouseX and mouseY start in the corner
    int y = (mouseY - height/2);
    mouseAngle = (atan2(x, y) - radians(90)) * -1; //offset it a bit
    if(mouseAngle < 0){
      mouseAngle = radians(360) + mouseAngle;
    }
    
    //get the distance from the center of the circle to the mouse
    float mouseDist = dist(width/2, height/2, mouseX, mouseY);
    
    float currentAngle = lastAngle + radians(angles[i]);
     //draw each section of the circle
    arc(width/2, height/2, diameter, diameter, lastAngle, currentAngle);
    //fill the corresponding section with a colour
    if(mouseAngle > lastAngle && mouseAngle <= currentAngle && mouseDist <= diameter/2){
      //the text to display
      switch(i){
        case 0:
          displayText = "First segment";
          break;
        case 1:
          displayText = "Second segment";
          break;
        case 2:
          displayText = "Third segment";
          break;
        case 3:
          displayText = "Fourth segment";
          break;
        case 4:
          displayText = "Fifth segment";
          break;
        case 5:
          displayText = "Sixth segment";
          break;
        default:
          displayText = "Segment " + (i+1);
          break;
      }
      //this is the hover over colour
      fill(12,32,180);
       //draw each section of the circle
    arc(width/2, height/2, diameter + 20, diameter + 20, lastAngle, currentAngle);
    }
    
   
    
    
    //the text colour
    fill(56,12,158);
    //the text size
    textSize(20);
    
    
    //displaying the text and updating the angle
    text(displayText, mouseX, mouseY);
    lastAngle += radians(angles[i]);
  }
}
